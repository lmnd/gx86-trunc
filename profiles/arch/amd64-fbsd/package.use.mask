# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/profiles/arch/amd64-fbsd/package.use.mask,v 1.18 2014/04/16 23:39:20 johu Exp $

# Johannes Huber <johu@gentoo.org> (16 Apr 2014)
# unkeyworded deps
kde-base/nepomuk-core migrator

# Pacho Ramos <pacho@gentoo.org> (02 Dec 2013)
# Missing keywords, bug #493156
media-libs/libgphoto2 serial

# nvidia-driver uses acpid
x11-drivers/nvidia-drivers acpi

# Needs emul libs and the package isnt multilib at all for FreeBSD x86_64
x11-drivers/nvidia-drivers multilib

# requires nvidia-cg-toolkit which is not available on bsd
# see http://developer.nvidia.com/cg-toolkit-download
media-libs/libprojectm video_cards_nvidia
